import AWS from 'aws-sdk';
import { v4 } from 'uuid';
import { CreateBook } from '../../generated/schema';

async function createBook(_: unknown, { input }: { input: CreateBook }) {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();
  const id = v4();

  const params = {
    TableName: process.env.ITEM_TABLE ? process.env.ITEM_TABLE : '',
    Item: {
      PK: `AUTHOR`,
      SK: `AUTHORS#${id}`,
      GSI1PK: `BOOKS`,
      GSI1SK: `BOOK#${id}`,
      name: input.name,
      author: input.author,
      price: input.price,
      publishingYear: input.publishingYear,
      publisher: input.publisher,
      page: input.page,
      description: input.description,
      genre: input.genre,
    },
  };

  await dynamoDb.put(params).promise();

  return {
    ...input,
    id,
  };
}

export default createBook;
