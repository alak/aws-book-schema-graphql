import AWS from 'aws-sdk';
import { v4 } from 'uuid';
import { CreateBook } from '../../generated/schema';

async function createAuthor(_: unknown, { input }: { input: CreateBook }) {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();
  const id = v4();

  const params = {
    TableName: process.env.ITEM_TABLE ? process.env.ITEM_TABLE : '',
    Item: {
      PK: `USER${id}`,
      SK: 'USERCONTENT',
      GSI1PK: 'CONTENT',
      GSI1SK: 'USER',
      firstName: input.name,
      lastName: input.price,
    },
  };

  await dynamoDb.put(params).promise();

  return {
    ...input,
    id,
  };
}

export default createAuthor;
