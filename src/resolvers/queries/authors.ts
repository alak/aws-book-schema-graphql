import AWS from 'aws-sdk';
import { Author, Authors } from '../../generated/schema';
//import author from './author';

async function authors(
  _: unknown,
  input: { author: Authors }
): Promise<Author> {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: process.env.ITEM_TABLE ? process.env.ITEM_TABLE : '',
    KeyConditionExpression: 'PK = :hkey',
    ExpressionAttributeValues: {
      ':hkey': `${input.author}`,
    },
  };

  const { Items } = await dynamoDb.query(params).promise();

  const authors =
    Items &&
    Items.map((authors) => {
      return authors;
    });
  console.log(authors);

  return authors;
}

export default authors;
