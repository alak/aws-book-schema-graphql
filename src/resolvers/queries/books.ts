import AWS from 'aws-sdk';
import { Book } from '../../generated/schema';

async function books(parent: Book, args): Promise<Book> {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: process.env.ITEM_TABLE ? process.env.ITEM_TABLE : '',
    IndexName: 'GSI1',
    KeyConditionExpression: 'GSI1PK = :hkey',
    ExpressionAttributeValues: {
      ':hkey': `${args.book}`,
    },
  };

  const { Items } = await dynamoDb.query(params).promise();

  const allBooks =
    Items &&
    Items.map((book: Book) => {
      return book;
    });

  return allBooks;
}

export default books;
