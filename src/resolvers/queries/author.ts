import AWS from 'aws-sdk';

async function author(parent, args) {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: process.env.ITEM_TABLE ? process.env.ITEM_TABLE : '',
    KeyConditionExpression: 'PK = :hkey and SK= :sk',
    ExpressionAttributeValues: {
      ':hkey': args.PK,
      ':sk': `AUTHORS#${args.SK}`,
    },
    Limit: 1,
  };

  const { Items } = await dynamoDb.query(params).promise();

  return Items;
}

export default author;
