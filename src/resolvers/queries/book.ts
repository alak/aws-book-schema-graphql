import AWS from 'aws-sdk';

async function book(parent, args) {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();

  const params = {
    TableName: process.env.ITEM_TABLE ? process.env.ITEM_TABLE : '',
    IndexName: 'GSI1',
    KeyConditionExpression: 'GSI1PK = :hkey and GSI1SK = :sk',
    ExpressionAttributeValues: {
      ':hkey': args.GSI1PK,
      ':sk': `BOOK#${args.GSI1SK}`,
    },
    Limit: 1,
  };

  try {
    const { Items } = await dynamoDb.query(params).promise();

    return Items[0];
  } catch (err) {
    console.log('error fetching from DDB: ', err);
    return err;
  }
}

export default book;
