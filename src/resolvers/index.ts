import createBook from './mutations/createBook';
import author from './queries/author';
import authors from './queries/authors';
import book from './queries/book';
import books from './queries/books';

const resolvers = {
  Query: {
    books,
    authors,
    author,
    book,
  },
  Mutation: {
    createBook,
  },
};

export default resolvers;
