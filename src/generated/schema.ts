export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Author = {
  __typename?: 'Author';
  authorName: Scalars['String'];
  book?: Maybe<Array<Maybe<Book>>>;
};

export enum Authors {
  Author = 'AUTHOR',
}

export type Book = {
  __typename?: 'Book';
  author: Array<Maybe<Author>>;
  description?: Maybe<Scalars['String']>;
  genre?: Maybe<Array<Maybe<Genre>>>;
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  page?: Maybe<Scalars['Int']>;
  price?: Maybe<Scalars['String']>;
  publisher?: Maybe<Scalars['String']>;
  publishingYear?: Maybe<Scalars['String']>;
};

export type CreateAuthor = {
  authorName: Scalars['String'];
};

export type CreateBook = {
  author: Array<InputMaybe<CreateAuthor>>;
  description?: InputMaybe<Scalars['String']>;
  genre?: InputMaybe<Array<InputMaybe<Genre>>>;
  name?: InputMaybe<Scalars['String']>;
  page?: InputMaybe<Scalars['Int']>;
  price?: InputMaybe<Scalars['String']>;
  publisher?: InputMaybe<Scalars['String']>;
  publishingYear?: InputMaybe<Scalars['String']>;
};

export enum Genre {
  Adventure = 'adventure',
  Drama = 'drama',
  Scifi = 'scifi',
}

export type Mutation = {
  __typename?: 'Mutation';
  createBook?: Maybe<Book>;
};

export type MutationCreateBookArgs = {
  input: CreateBook;
};

export type Query = {
  __typename?: 'Query';
  author?: Maybe<Array<Maybe<Author>>>;
  authors?: Maybe<Author>;
  book?: Maybe<Book>;
  books?: Maybe<Array<Maybe<Book>>>;
};

export type QueryAuthorArgs = {
  PK?: InputMaybe<Scalars['String']>;
  SK?: InputMaybe<Scalars['String']>;
};

export type QueryAuthorsArgs = {
  author?: InputMaybe<Authors>;
};

export type QueryBookArgs = {
  GSI1PK?: InputMaybe<Scalars['String']>;
  GSI1SK?: InputMaybe<Scalars['String']>;
};

export type QueryBooksArgs = {
  book?: InputMaybe<Scalars['String']>;
};
