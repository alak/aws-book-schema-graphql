import { gql } from 'apollo-server-lambda';

const typeDefs = gql`
  enum Genre {
    adventure
    drama
    scifi
  }

  enum Authors {
    AUTHOR
  }

  type Query {
    author(PK: String, SK: String): [Author]
    authors(author: Authors): Author
    book(GSI1PK: String, GSI1SK: String): Book
    books(book: String): [Book]
  }

  type Mutation {
    createBook(input: CreateBook!): Book
  }

  type Author {
    authorName: String
    book: [Book]
  }

  type Book {
    id: ID!
    name: String
    price: String
    publishingYear: String
    publisher: String
    author: [Author]
    description: String
    page: Int
    genre: [Genre]
  }

  input CreateBook {
    name: String
    price: String
    publishingYear: String
    publisher: String
    author: [CreateAuthor]!
    description: String
    page: Int
    genre: [Genre]
  }

  input CreateAuthor {
    authorName: String!
  }
`;
export default typeDefs;
